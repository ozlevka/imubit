# Imubit Exam
Your task is to build a containerized logging environment in Kubernetes, with the following minimal
components:
1. Monitored containers - At least 2 "dummy" containers that produce logs to stdout/stderr in random intervals.
2. Database - A suitable database of your choice with persistent storage.
3. (Optional) Web server - Displays the logs from the monitored containers.
4. For each one of the Monitored containers, their output should be written to the database, with their unique id and timestamp.

## Output requirements:
1. You should provide a script that will deploy your solution into an existing k8s cluster.
2. Your script will be executed in a Linux machine with kubectl installed and configured.
3. A short README with installation instructions and an overview of your solution.
4. If you choose not to implement the Web server, please provide instructions
for viewing the logs stored in the database.
5. You may push the solution to a private git repository or send it zipped over email to careers@imubit.com
Effort Required
The basic challenge should take 1-2 days of work, to be completed within 1 week.

# Solution is based on ELK stack:
1. In role of database Elasticsearch server (statefullset)
2. In role of Web Server Kibana
3. In role of log collector filebeat...
4. Because no log editing is included we not need logstash...

Installation manager is Helm

Installation orchestartor is Ansible

Solution is fully dockerized to reduce configuration collisitons. For build/rebuild docker images run ```./_build.sh```.
Be sure change docker image repo to ```docker push [repo you have permissions]/[container name]```  


## Installation Instructions
1. This code tested on cluster created into AWS cloud by [rancher](https://rancher.com/products/rancher).
2. If you not use [EKS](https://aws.amazon.com/eks) please be sure to provide [cluster cloud provider](https://kubernetes.io/docs/concepts/cluster-administration/cloud-providers/). Without this setting statefulset are failed during deploy.
3. After create cluster be sure to update ```~/.kube/config```.
4. Please be sure to install [docker](https://docs.docker.com/install) on your machine.
5. Installtion script tested on Ubuntu 18.04.
6. For deploy clone this repository and just run ```./install.sh``` script. For detailed usage information please run ```./install.sh --help```
7. After infrastructure are created link to kibana loadbalancer will be printed to your command line. Be sure open 5601 port in security group.
8. For uninstall tested stack please run ```./install.sh -u```

## Thank you for attention and grate exercise

